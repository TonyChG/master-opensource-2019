# OS, kernel & `systemd`

- [OS, kernel & `systemd`](#os-kernel--systemd)
- [I. Kernel & OS](#i-kernel--os)
  - [1. Concept](#1-concept)
  - [2. How to interact with kernel and hardware ?](#2-how-to-interact-with-kernel-and-hardware)
    - [syscalls](#syscalls)
    - [Interruptions](#interruptions)
    - [Izi schema](#izi-schema)
  - [3. Quelques technologies du kernel](#3-quelques-technologies-du-kernel)
    - [Namespaces](#namespaces)
    - [cgroups](#cgroups)
    - [D-Bus](#d-bus)
  - [4. Système d'exploitation](#4-syst%c3%a8me-dexploitation)
- [II. `systemd`](#ii-systemd)
  - [1. Rôles](#1-r%c3%b4les)
    - [PID 1](#pid-1)
    - [Gestion de service](#gestion-de-service)
  - [Et plus encore](#et-plus-encore)
  - [2. Fonctionnalités avancées](#2-fonctionnalit%c3%a9s-avanc%c3%a9es)
    - [Centralisation des logs](#centralisation-des-logs)
    - [Sécurité renforcée des services](#s%c3%a9curit%c3%a9-renforc%c3%a9e-des-services)
    - [Fined-grained containers](#fined-grained-containers)
  - [3. Controversy](#3-controversy)

# I. Kernel & OS

## 1. Concept

> *On survole à peine le fonctionnement, c'est simplement pour introduire le fonctionnement de ces différents éléments.*

Le kernel ou "noyau" est la pièce centrale de tout système d'exploitation. C'est à la charge du kernel de discuter avec le CPU et le reste du hardware.  

Il communique avec ces composants *via* des bus dédiés, en utilisant des protocoles dédiés. Il connaît ces protocoles à l'aide de *drivers*.  

**Le kernel expose des APIs pour pouvoir utiliser le matériel de façon programmatique** (avec du code quoi :) ).

Le système d'exploitation est un ensemble d'outil qui viennent se greffer autour du kernel, en utilisant ses APIs, afin de fournir des fonctionnalités accessibles à un humain. On parle ici principalement de :
* utilisation du matériel (clavier, souris, écran, etc)
* gestion de fichiers
* **exécution de processus** et gestionnaire de fenêtres
* accès au réseau

**Le système d'exploitation expose des shells** (littéralement "coquilles") afin d'interagir avec lui, et par son biais, avec le kernel :
* les GUIs
  * Gnome, KDE, xfce, i3, ~~`explorer.exe`~~, etc
* les CLIs
  * `sh`, `dash`, `bash`, ~~`Powershell`~~, etc

On distingue souvent en deux parties le système : 
* **le kernel-space**
  * les processus utilisés par le kernel 
    * fonctionnalités essentielles comme un scheduler de tâches
    * fonctionnalités annexes comme l'utilisation de certains modules ou drivers
* **le user-space**
  * ce que le commun des mortels et les adminsys utilisent
    * apache, NGINX, mysql, firefox, VLC, toussa.
    * la session utilisateur elle-même aussi

## 2. How to interact with kernel and hardware ?

### syscalls

Les *syscalls* ou *appels système* sont des fonctions du kernel (du C donc) qui permettent de lui envoyer des messages, des ordres. Ce sont les fonctionnalités atomiques d'un système informatisé. Par exemple, pour Linux :
* lire un fichier : `read()`
* écrire dans un fichier : `write()`
* créer un périphérique ou un dossier ou un fichier : `mknod()`
* exécuter un processus : `fork()`
* gérer les permissions d'un fichier : `chmod()`
* envoyer un signal à un processus : `kill()`
* etc.

Tous les programmes qu'on utilise, du plus simple au plus complexe, peuvent être décomposés en une suite d'appel système.

### Interruptions

Les *interruptions système* fonctionnent d'une façon un peu similaire aux *syscalls*. La différence étant que le destinataire n'est pas le kernel, mais le CPU lui-même. Autre différence : les interruptions peuvent être software (et venir du kernel) ou hardware (et venir d'un périphérique).  

**Les interruptions sont des ordres envoyés au CPU, et qui monopolise complètement son attention. Ce sont des messages à traiter de façon urgente.**  

Par exemple : 
* un GPU qui a fini de dessiner une frame
* un nouveau périphérique est branché
* une trame réseau est reçue
* processus à haute priorité lancé sur le système

### Izi schema

![Izi schema (HW, kernel, OS, user)](./pics/hw-os-kernel-user.png)

## 3. Quelques technologies du kernel

Avant d'appréhender `systemd` il est nécessaire d'introduire plusieurs autres mécanismes du kernel.  

Il faut rappeler que le kernel a la charge d'entretenir la liste des processus (notamment en leur fournissant un espace en RAM et en demandant au processeur d'exécuter leurs instructions) et de leur permettre de communiquer. Communiquer entre eux, et communiquer avec les ressources du système (disques, réseau, etc.).

### Namespaces

Les *namespaces* constituent une fonctionnalité du noyau Linux. Ce sont des espaces où vivent les processus. **L'utilisation de namespaces permet d'isoler certaines partie du système pour un ou plusieurs processus donné(s).**  
* isolation des ressources système (ressources OS, PAS les ressources matérielles)
* les namespaces ont une structure arborescente
  * lorsque vous démarrez votre système, vous êtes dans les namespaces "principaux" ou "parents"
  * si vous créez de nouveaux namespaces, étant dans le namespace parent, vous voyez tout
  * mais les namespaces enfants ne voient pas ce qu'il y au-dessus d'eux
* on peut créer un processus dans un nouveau namespace donné avec l'appel système `unshare()` (la commande `unshare` permet de l'utiliser depuis la ligne de commande)

**Il existe 7 types de namespaces**
* **`mnt` pour "mount"**
  * isole l'arbre de montage
  * output différent avec `df -h` ou `mount`
* **`pid` pour "process ID"**
  * isole l'arbre de processus
  * output différent avec un `ps -ef`
* **`ipc` pour "inter-process communication"**
  * isole certains canaux de communication inter-processus
  * comme les bus D-Bus
* **`net` pour "network"**
  * isole la pile réseau
  * le nouveau processus n'a qu'une interface loopback (sauf configuré explicitement autrement)
  * output de `ip a` différent
* **`uts`**
  * isolation de la gestion des noms de domaine
  * output de `hostname` différent
* **`user`**
  * isolation de l'arbre utilisateur
  * pour que les utilisateurs des nouveaux namespaces puissent être visibles et exister dans le namespace parent, un système de mapping est mis en place
  * `root` dans un namespace enfant correspond à un utilisateur non-privilégié dans le namespace parent
* **`cgroup`**
  * arborescence de cgroups différentes
  * `/sys/fs/cgroup` différent

Par exemple, si deux processus (disons un terminal et un firefox) sont dans deux namespaces réseau différents, ils n'auront pas accès aux mêmes interfaces.

> C'est en partie sur les namespaces que repose Docker par ailleurs. Vous voyez les processus qu'un conteneur lance depuis l'hôte, mais lui, le conteneur ne peut pas voir les processus de l'hôte. Avec ça aussi qu'un conteneur a ses propres carte réseau. Et avec ça qu'il a son propre filesystem.

### cgroups

Le terme *cgroup* désigne un mécanisme kernel de labellisation de processus et restriction de ressources appliquées aux processus.  

En clair, cela permet de rassembler les processus en groupe de processus. Pour ensuite potentiellement appliquer des restrictions d'accès aux ressources de la machines comme : 
* utilisation CPU
* utilisation RAM
* I/O disque
* utilisation réseau
* utilisation namespaces (later)

L'utilisation généralisée des cgroups au sein des systèmes GNU/Linux permet un plus grand contrôle sur les processus et une administration plus aisée.  
D'autres fonctionnalités qui en découlent sont le monitoring des processus par cgroups ou la priorisation de l'accès au ressources de la machines. 

> Cela permet par exemple une attribution plus fine de ressources qu'avec des mécanismes comme le *oom_score* (pour l'utilisation de la RAM) ou le *nice_score* (pour l'utilisation du CPU). Google it if ou don't know it.

**Les cgroups possèdent une structures arborescentes, comme une arborescende fichiers. Mais ua lieu de ranger des fichiers, on range des processus.** 🔥🔥🔥

### D-Bus

*D-Bus* est une technologie d'IPC : elle permet la communication entre les processus. 

Ses caractéristiques sont :
* c'est un démon (on peut vérifier qu'il tourne avec un `ps -ef | grep dbus`
* permet l'identification des ressources (et donc l'autorisation d'accès)
* il définit un protocole standard et unifié pour communiquer entre les processus
* *on peut dire que D-Bus est aux sockets UNIX ce que les APIs REST sont à TCP*  

Son fonctionnement :
* les processus s'abonnent à un bus donné (user ou system par défaut, on peut en créer d'autres)
* les processus peuvent
  * émettre des signaux
    * évènement général envoyé en broadcast qui permet à tout le monde de savoir que cet évènement a eu lieu
  * exécuter des méthodes
    * les méthodes appartiennent à un objet donné et permettent d'effectuer une action
    * on peut avoir des valeurs d'entrées (paramètres) et de sortie (valeur de retour)
* les échanges ont un format binaire standardisé (perfs, analyse, etc.)
* les processus sont nommés
  * forcément un ID unique
  * possibilité de définir un nom (car le human-readable c'est quand même cool)

Pour appeler une méthode d'un processus, il faut :
* connaître le nom du processus sur le bus 
* connaître son *path*
* connaître l'*interface* qui implémente la méthode que l'on souhaite appeler
* connaître le nom de la méthode

D-Bus  est dit "introspectable" car il propose des mécanismes natifs pour monitorer son état, et requêter des objets (pour lister les méthodes qu'ils exposent par exemple). 

[Cet article](http://0pointer.net/blog/the-new-sd-bus-api-of-systemd.html) contient une très bonne overview de ce qu'est D-Bus et de comment s'en servir.

---

Exemple d'introspection (exploration d'un bus donné) avec `busctl` :

```
# Lister les informations liés aux bus du système
$ sudo busctl list
$ sudo busctl list --system

# Repérage d'un objet à introspecter, par exemple org.freedesktop.NetworkManager

# Introspection de l'objet
$ sudo busctl status org.freedesktop.NetworkManager
$ sudo busctl monitor org.freedesktop.NetworkManager

# Récupérer les objets (path) du service observé
$ sudo busctl tree org.freedesktop.NetworkManager

# Obtention de toutes les interfaces, méthodes, signaux et propriétés mis à disposition par org.freedesktop.NetworkManager
$ sudo busctl introspect org.freedesktop.NetworkManager /org/freedesktop/NetworkManager

# Récupération de la valeur d'une propriété
$ sudo busctl get-property org.freedesktop.NetworkManager /org/freedesktop/NetworkManager org.freedesktop.NetworkManager AllDevices

# Appel d'une méthode
$ sudo busctl call org.freedesktop.NetworkManager /org/freedesktop/NetworkManager org.freedesktop.NetworkManager GetAllDevices
```

## 4. Système d'exploitation

Le rôle du système d'exploitation est d'exposer les fonctionnalités du kernel d'une façon utilisable par un humain. Cela veut dire, entre autres, d'effectuer les bons syscalls, les bonnes interruptions système et organiser un écosystème cohérent, afin que nous puissions utiliser le matériel.

Les fonctionnalités élémentaires peuvent être rassemblées dans ce qu'on pourrait appeler le "system-space". A mi-chemin entre le userspace et le kernelspace, il contient les fonctionnalités élémentaires d'un OS moderne :
* gestion du réseau
  * configuration interfaces
  * DNS
* gestion du stockage 
  * disques et autres devices
  * filesystems
  * chiffrement
* gestion du temps
* gestion des IPC
* gestion de processus
* logs
* gestion de session
* etc.

<div align="center">

![systemspace](pics/systemspace.png)

</div>

# II. `systemd`

`systemd` est un outil principalement développé par Lennart Poettering, dont le développement [a commencé en 2010](http://0pointer.de/blog/projects/systemd.html).  

Il a depuis été largement adopté par de grandes familles de distributions GNU/Linux, parmis lesquelles : Debian (Ubuntu, etc), RedHat (RHEL, CentOS, Fedora, ScientificLinux, etc), ArchLinux.  

Il a été grandement controversé pour plusieurs raisons qui seront détaillés dans [une section dédiée]((#controversy)).  

`systemd` a plusieurs rôles au sein du système d'exploitation : 
* PID 1
* gestionnaire de service
* "system-space" manager ?

Il est réputé pour : 
* **améliorer la vitesse du boot**
  * parallélisation des tâches
  * event-based
* **unifier la gestion du système** et spécifiquement, la gestion des services système
  * plus de shellscripts pour la gestion de services
  * centralisation des logs
* **proposer un écosystème d'outil pour gérer le "system-space"**
  * réseau, gestion du temps, des noms de domaines, du DNS, etc.
* proposer des **fonctionnalités modernes et avancées pour la gestion de processus**
  * sécurité
  * performances
  * stabilité

## 1. Rôles

### PID 1

`systemd` est le premier processus lancé par le kernel une fois qu'il a fini d'être chargé en RAM et qu'il a effectué quelques étapes préliminaires.  

Il a pour principales tâches de :
* lancer les autres processus nécessaire au bon fonctionnement du système
  * `systemd` entretient par la suite un lien étroit avec ces "processus système"
* adopter les processus orphelins
  * les processus dont le père est mort :o
* proposer une session utilisateur

### Gestion de service

`systemd` est un gestionnaire de service. Il permet le lancement de processus de façon supervisé, et propose des outils pour interagir avec ceux-ci. La plupart des interactions sont réalisées avec la commande `systemctl`.  

On peut lister les services gérés par `systemd` avec :
```
$ systemctl -t service
$ systemctl -t service --all
```

Quelques commandes `systemctl` élémentaires :
```
# Démarre le service
$ systemctl start <SERVICE>

# Affiche l'état du service
$ systemctl status <SERVICE>

# Arrête le service
$ systemctl stop <SERVICE>

# Affiche l'unité systemd lié au service
$ systemctl cat <SERVICE>
```

`systemd` permet :
* d'organiser le démarrage des services avec des liens de dépendances
* de surveiller le cycle de vie des services
  * surveille la liste des processus
  * permet de redémarrer automatiquement des processus
* une interaction avec les services lancés avec `systemctl`

## Et plus encore

`systemd` permet une gestion avancée des services grâce à la mise en place de plusieurs concepts et outils, ou collaboration étroite avec d'autres outils du kernel et de l'OS :
* kernel
  * namespaces
    * isolation des processus
  * cgroups
    * monitoring des processus
    * restriction d'accès aux ressources (RAM, I/O disque, CPU, réseau, filesystem, etc)
  * autres
    * MAC mechanisms (SELinux, Smack, Apparmor)
    * seccomp
    * BPF
    * D-Bus
* gestion du "system-space"
  * gestion de DNS
    * `systemd-resolved`
  * gestion de la pile réseau
    * `systemd-networkd`
  * gestion des noms de domaines
    * `hostnamectl`
  * gestion des tâches planifiées
    * unités `timer`
  * gestion du temps
    * `systemd-timesyncd`
* concepts
  * centralisation des logs
    * `journald`
  * event-based activation
    * activation par socket, fichier créé, etc.
  * fortes relations de dépendances

`systemd` est aussi un outil tout indiqué pour des OS orientés cloud-native (pour servir de base dans environnements Cloud).  
En effet il est possible de provisionner entièrement la configuration système d'une machine uniquement en passant par `systemd` (montage des filesystems, y compris filesystems réseau, gestion de services, gestion des DNS, etc).  

> Avec des outils comme cloud-init ou Ignition, il est possible de passer une configuration `systemd` au boot de la machine, et ainsi de n'avoir besoin de déployer aucune configuration (approche stateless).

## 2. Fonctionnalités avancées

### Centralisation des logs

`journald` est l'outil de centralisation de logs fourni avec `systemd`.  

Les logs sont stockés dans un format binaire, ce qui peut paraître nonsense au début mais qui fournit en réalité beaucoup d'avantages :
* le timestamp est géré de façon interne à journald, on peut donc requêter le journal relativement à n'importe quel format de date ou fuseau horaire
* on peut requêter le journal avec des filtres très fins et puissants
* on peut exporter les logs dans plusieurs formats (dont json)

### Sécurité renforcée des services

`systemd` peut nativement faire appel à des mécanismes du noyeau et de l'OS afin de renforcer le niveau de sécurité des unités.  

Cela se fait à travers la syntaxe simple et unifiée de déclaration d'unités systemd. Ici est surtout question des unités de type `service`.

* tous les processus gérés par `systemd` sont lancés dans des cgroups, selon une hiérarchie rigoureuse
  * un arbre pour le système
  * un arbre pour les utilisateurs
    * sub-divisé en un arbre pour chaque utilisateur
* les services peuvent être sandboxés de façon très fine grâce aux namespaces
  * isolation du répertoire `/tmp`, `/dev` ou msquage complet de `/home` et `/root` par exemple
  * isolation des cartes réseau du système
* on peut restreindre les capacités d'un service
  * en filtrant les appels système qu'il passe (seccomp)
  * en limitant ses capabilities
  * en paramétrant un mécanisme MAC (SELinux, Smack, AppArmor)

### Fined-grained containers

Ayant été construit avec les cgroups, namespaces et capabilities comme first-class citizen, `systemd` est en mesure de créer des conteneurs LXC-like ou Docker-like.  

En plus de simplement posséder des fonctionnalités similaires, il permet de les mettre en place de façon différente suivant le besoin : 
* le plus courant est d'ajouter à des définitions d'unités de typer *service* des clauses de sécurité
* on peut aussi utiliser les commandes 
  * `systemd-run` : orienté sandboxing one-shot
  * `systemd-nspawn` : orienté testing avec namespaces
* portable services 
  * conteneurs contrôlés par l'OS et de façon plus fine qu'un conteneur Docker par exemple
  * adapté à l'isolation de services qui sont des extensions de l'OS plus que des applications qui tournent par dessus l'OS
    * par exemple un DNS spécifique
    * un outil de chiffrement de partition
  * il existe de "profils" prédéfinis permettant un sandboxing plus rapide qu'avec une simple unité `systemd`

## 3. Controversy

* **"`systemd` is monolithic and bloated !"**
  * un peu, mais pas trop. L'ensemble des composants sont modulaires, et on peut s'en servir de façon indépendante. `systemd` ne peut être qu'un simple gestionnaire de service et laisser les fonctionnalités comme la gestion du temps à un démon classique
  * un peu vrai quand il s'agit d'enlever (ou ajouter) des fonctionnalités : il faut recompiler. `systemd` est un gros morceau de code compilé, dont on ne peut pas modifier facilement le comportement (contrairement à un OpenRC par exemple)
* **"it's breaking UNIX philosophy !"** (faire une seule chose et la faire bien)
  * well, not really. `systemd` embarque tout un tas de composants qui sont développés indépendamment, mais simplement embarqué dans `systemd`, comme udev
* **"it's too complex !"**
  * un peu complexe oui, mais pas plus complexe que l'ancienne gestion, avec beaucoup de spécificités et cas particuliers suivant les OS et versions d'OS, et des shellscripts partouuuut.
